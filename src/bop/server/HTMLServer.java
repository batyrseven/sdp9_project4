package bop.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

import bop.common.Globals;
import bop.common.LoggerFactory;
import bop.seating.SeatingManager;

/**
 * An HTML server which implements a substet of the http protocol.  
 * This server is used to serve up a dynamically created html wait list page. * 
 * This class extends Thread so that the server is run
 * separately from the SeatingManager process which initiates it.
 * 
 * @see WaitListWriter
 * @see SeatingManager
 * 
 * @author sgl
 * 
 */
/*
 * INSTRUCTOR NOTES:
 * As scaffolding projects for more advanced students, students might:
 * - make this a multi-threaded server.  As implemented, this server handles one request
 * 	at a time in a single thread.  It could be enhanced to handle each request in its
 * 	own thread.
 * - implement additional file types
 * - implement additional return status codes.
 * - implement additional header properties (ie.Content-Length, Content-Location, Content-Encoding, Content-Language, etc.)
 *    see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
 */
public class HTMLServer extends Thread {

	/** The name of the web server */
	private static final String BIZNAME = Globals.get("BizName");
	/** The root directory in which all of the content resides */
	public static final String ROOT_DIR = Globals.get("WebRootDir");
	/** The port this server monitors */
	private static final String LISTEN_PORT = Globals.get("HtmlServerPort");
	/** The port this server monitors */
	private static final String WAITLISTFILE = Globals.get("WaitListFile");
	/** The port this server monitors */
	private static final String NOWAITLISTFILE = Globals.get("NoWaitListFile");
	/** The logger for this class. */
	private static final Logger LOGGER = LoggerFactory.getLogger();
	/** The wait list html page writer */
	private WaitListWriter writer;

	/**
	 * Default constructor. The HTML server runs in its own thread and handles
	 * each request sequentially, blocking while each client accesses the
	 * server.
	 */
	public HTMLServer() {
		writer = new WaitListWriter();
		this.start();
	}

	/**
	 * Run the server.
	 */
	@Override
	public void run() {
		// declare the socket
		ServerSocket serversocket = null;
		LOGGER.info("The " + BIZNAME + " HTML server is running on port "
				+ LISTEN_PORT + ".");

		// try to bind the socket to the specified port
		try {
			// make a ServerSocket and bind it to given port,
			serversocket = new ServerSocket(Integer.parseInt(LISTEN_PORT));
		} catch (Exception e) { // catch any errors and log them
			LOGGER.severe("Fatal Error in run: " + e.getMessage() + "\n");
			return;
		}

		// loop, waiting for connections, processing request and sending
		// response
		while (true) {
			try {
				// wait/block until someone connects to the port we
				// are listening to
				Socket connectionsocket = serversocket.accept();
				// figure out what ip address the client comes from and log it
				InetAddress client = connectionsocket.getInetAddress();

				if (client.getHostName().equals(client.getHostAddress())) {
					LOGGER.info("Client at " + client.getHostAddress()
							+ " issued request.");
				} else {
					LOGGER.info(client.getHostName() + " at "
							+ client.getHostAddress() + " issued request.");
				}
				// read the http request from the client on the socket
				BufferedReader input = new BufferedReader(
						new InputStreamReader(connectionsocket.getInputStream()));
				// prepare a output stream to the client to send back our
				// response
				// (header + requested file) to the client.
				DataOutputStream output = new DataOutputStream(
						connectionsocket.getOutputStream());

				// handle the specific http request
				httpHandler(input, output, client);
			} catch (Exception e) { // catch any errors, and log them
				LOGGER.severe("Error in run:" + e.getClass().getName() + " - "
						+ e.getMessage() + "\n");
			}

		} // loop, wait for next request
	}

	/**
	 * Handle http requests.
	 * 
	 * The two types of requests we will handle GET /index.html HTTP/1.0 HEAD
	 * /index.html HTTP/1.0
	 * 
	 * Other requests could be made, but this is a simplified server with
	 * limited functionality.
	 * 
	 * NOTE that there is a security issue when passing the untrusted string
	 * "path" to FileInputStream. The client can access all files to which the
	 * server process has read access.
	 * 
	 * @param request
	 *            the input request data stream
	 * @param reply
	 *            the output reply data stream
	 * @param client
	 *            the internet address of the client making the request
	 */
	private void httpHandler(BufferedReader request, DataOutputStream reply,
			InetAddress client) {

		String requestType = "";
		String requestFile = "";
		// String requestHttpVersion = null;
		try {
			// read the request from the input stream
			String requestStr = request.readLine();

			// requestStr contains something like
			// "GET /index.html HTTP/1.0 ......."
			// tokenize the string to pull out the various components
			String[] tokens = requestStr.split(" ");
			requestType = tokens[0];
			requestFile = tokens[1];
			// requestHttpVersion = tokens[2];

			// if request is not a GET or HEAD, return a 501 (Not Implemented)
			// error.
			if (!(requestType.equals("GET") || requestType.equals("HEAD"))) {
				try {
					reply.writeBytes(constructHttpHeader(501, null));
					reply.close();
					return;
				} catch (Exception e) { // if some error happened, catch it
					LOGGER.severe("Error in httpHandler with 501: "
							+ e.getMessage());
				} // log error
			}

			// if no file is specified, default to index.html
			if (requestFile.length() == 1) {
				requestFile = File.separator + "index.html";
			}
			// prepend the request with the root directory for all web pages on
			// this server
			requestFile = ROOT_DIR + requestFile;
		} catch (Exception e) {
			LOGGER.severe("Error in httpHandler getting requested file: "
					+ e.getMessage() + "\n");
		} // catch any exception

		LOGGER.info("Client requested "
				+ new File(requestFile).getAbsolutePath());
		FileInputStream requestedFile = null;

		// we have the filename to the file which was requested
		if (requestFile.endsWith(WAITLISTFILE)) {
			// identify network location and/or machine
			String ipAddr = client.getHostAddress();
			String[] ipBits = ipAddr.split("\\.");
			if (!(ipBits[0].equals("192") || client.isSiteLocalAddress() || // client
																			// on
																			// server
					ipBits[0].equals("127") || client.isLoopbackAddress())) { // client
																				// on
																				// LAN
				requestFile = requestFile.replace(WAITLISTFILE, NOWAITLISTFILE);
				LOGGER.info("Non-local client request for " + WAITLISTFILE
						+ " redirected to " + NOWAITLISTFILE);
			}
		}

		// try to open the requested file
		try {
			requestedFile = new FileInputStream(requestFile);
		} catch (Exception e) {
			try {
				// if you could not open the file, send a 404 (Not Found)
				reply.writeBytes(constructHttpHeader(404, null));
				// close the stream
				reply.close();
			} catch (Exception e2) {
				// if we can't write the simple error message
				// there's nothing we can do about it.
			}
			LOGGER.severe("Error in httpHandler in 404" + e.getMessage());
		} 

		// next find out what type of file was requested
		try {
			String contentType = "text/html";
			// determine the content type based on the filename suffix.
			if (requestFile.endsWith(".zip")) {
				contentType = "application/x-zip-compressed";
			} else if (requestFile.endsWith(".jpg")
					|| requestFile.endsWith(".jpeg")) {
				contentType = "image/jpeg";
			} else if (requestFile.endsWith(".gif")) {
				contentType = "image/gif";
			} else if (requestFile.endsWith(".pdf")) {
				contentType = "image/pdf";
			} // add more content types as needed. Not all are supported here.

			reply.writeBytes(constructHttpHeader(200, contentType));

			// If it was a HEAD request, we don't print any BODY
			// If it was a GET request, write the requested file contents
			// to the reply body.
			if (requestType.equals("GET")) {
				while (true) {
					// read the requested file and output it to the reply
					// on a byte per byte base.
					int b = requestedFile.read();
					if (b == -1) {
						break; // end of file
					}
					reply.write(b);
				}

			}
			// close the requested file and the reply
			reply.close();
			requestedFile.close();
		} catch (Exception e) {
			LOGGER.severe("Error in httpHandler fulfilling request: "
					+ e.getMessage());
		}
		LOGGER.info("Client request completed.");
	}

	/**
	 * Create an HTTP header for the response.
	 * 
	 * The header tells the browser whether the request succeeded or not as well
	 * as any reply content.
	 * 
	 * NOTE that not all return codes are created or supported here in this
	 * limited functionality server.
	 * 
	 * @param returnCode
	 *            the status of the request (success, failure)
	 * @param contentType
	 *            the type of the requested file being returned.
	 * @return the HTTP header
	 * 
	 *         NOTE that this server does not support persistent connections.
	 */
	private String constructHttpHeader(int returnCode, String contentType) {
		String header = "HTTP/1.0 ";
		// These are standard HTTP return codes.
		switch (returnCode) {
		case 200:
			header = header + returnCode + " OK";
			break;
		case 400:
			header = header + returnCode + " Bad Request";
			break;
		case 403:
			header = header + returnCode + " Forbidden";
			break;
		case 404:
			header = header + returnCode + " Not Found";
			break;
		case 500:
			header = header + returnCode + " Internal Server Error";
			break;
		case 501:
			header = header + returnCode + " Not Implemented";
			break;
		}
		header = header + "\r\n";

		// This server does not support persistent connections.
		header = header + "Connection: close\r\n";
		// include the name of this server
		header = header + "Server: " + BIZNAME + "\r\n";

		// Construct the right Content-Type for the header.
		// The browser dosen't look at the file extension.
		// It is the server's job to let the browser know
		// what kind of file is being transmitted.
		if (contentType == null) {
			contentType = "text/html";
		}
		header = header + "Content-Type: " + contentType + "\r\n";
		header += "Host: " + "myhost" + "\r\n";

		// mark the end of the header
		header = header + "\r\n";
		// return the header string
		return header;
	}

	/**
	 * Main entry point to run this server stand alone.
	 * @param args (not used)
	 */
	public static void main(String[] args) {
		new HTMLServer();
	}

}