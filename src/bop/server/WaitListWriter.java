package bop.server;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import bop.common.Globals;
import bop.seating.SeatingManager;
import bop.seating.SeatingObserver;

/**
 * The WaitListWriter dynamically creates a list of customers waiting to be seated in html format.  
 * This file is then served up by the HTML server to display the wait list on various monitors.
 * 
 * @see HTMLServer
 * 
 * @author sgl
 * 
 */
/*
 * INSTRUCTOR NOTES:
 * As scaffolding projects for more advanced students, students might:
 * - create a servlet which gets the data from the SeatingManager to dynamically create 
 * 	the html formatted wait list.
 */
public class WaitListWriter implements SeatingObserver {

	/** The business name to be displayed in the application window. */
	private final String BIZNAME = Globals.get("BizName");
	/** The port this server monitors */
	private final String WAITLISTFILE = Globals.get("WaitListFile");
	/** The root directory in which all of the content resides */
	private final String ROOT_DIR = Globals.get("WebRootDir");

	/**
	 * Default constructor.  
	 * Register this writer with the seating manager.
	 */
	public WaitListWriter() {
		SeatingManager.registerObserver(this);
	}

	/**
	 * Create an html table of the wait list names and positions.
	 * @param waitlist the list of names of parties waiting to be seated.
	 * @return a html table of wait list names and positions.
	 */
	private String createTable(Vector<String> waitlist) {
		StringBuffer buf = new StringBuffer(1000);
		int waitlistSize = waitlist.size();
		for (int i = waitlistSize; i > 0; i--) {
			int position = waitlistSize - i + 1;
			buf.append("<tr>");
			buf.append("<td><center>");
			buf.append("#" + position);
			buf.append("</center></td>");
			buf.append("<td><center>");
			buf.append(waitlist.elementAt(i - 1));
			buf.append("</center></td>");
			buf.append("</tr>\n");
		}
		return buf.toString();
	}

	/**
	 * Create the html head block for the waitlist table.
	 * @return the head block for the html file.
	 */
	private String getHead() {
		return "<HTML>\n<HEAD><center><b>\n" + BIZNAME + " Waiting List"
				+ "\n</b></center></HEAD>\n<BODY>\n<PRE>\n"
				+ "<table width=\"100%\" border>\n  " + "<tr><th>Position</th>"
				+ "<th>Name</th>" + "</tr>\n";
	}

	/**
	 * Seat a party.
	 * Implemented as SeatingObserver - no action is taken here.
	 */
	public void seat(String party, int tableNumber) {
	}

	/**
	 * Clear a table.
	 * Implemented as SeatingObserver - no action is taken here.
	 */
	public void clear(String party, int tableNumber) {
	}

	/**
	 * Add a party to the wait list.
	 * Implemented as SeatingObserver - no action is taken here.
	 */
	public void addToWaitList(String party, int position) {
	}

	/**
	 * Remove a party from the wait list.
	 * Implemented as SeatingObserver - no action is taken here.
	 */
	public void removeFromWaitList(String party) {
	}

	/**
	 * Update the wait list.
	 * The updated wait list is written out as an html table for display.
	 */
	public void updateWaitList(Vector<String> waitlist, int index) {

		File waitlistFile = new File(ROOT_DIR + File.separator + WAITLISTFILE);
		try {
			FileWriter writer = new FileWriter(waitlistFile, false);
			if (waitlist.size() == 0) {
				writer.write("<HTML>\n<HEAD><center><b>\n"
						+ BIZNAME
						+ "\n</b></center></HEAD>\n<BODY>\n<PRE><center>There is no waiting!\n</center></PRE></BODY>\n</HTML>\n");
			} else {
				writer.write(getHead());
				writer.write(createTable(waitlist));
				writer.write(getTail());
			}
			writer.close();
		} catch (IOException ioe) {
			SeatingManager.LOGGER.severe("Error writing to " + WAITLISTFILE
					+ ".");
		}
	}

	/**
	 * Create the html tail block for the waitlist table.
	 * @return the tail block for the html file.
	 */
	private static String getTail() {
		return "</table>\n  </PRE></BODY>\n</HTML>\n";
	}
}