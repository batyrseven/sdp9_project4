package bop.secure;

/**
 * The interface for authentication of a user in an application.
 * To use this, first create a AuthenticationGUI.  When the 
 * authentication process is complete, it will call 
 * authenticationComplete() with the authenticated name.
 * If the authentication failed or was canceled, the authenticatedUsername
 * will be null.
 * 
 * @see StaffNotifier
 * 
 * @author sgl
 *
 */
public interface AuthenticatedApplication {

	/**
	 * This method is called when the authentication process is 
	 * complete.
	 * @param authenticatedUsername the name of the authenticated user. 
	 * If the user is not authenticated, this will be null.
	 */
	public void authenticationComplete(String authenticatedUsername);
}
