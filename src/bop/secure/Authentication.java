package bop.secure;

import java.io.*;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import bop.common.LoggerFactory;
import bop.common.Utils;
import com.sun.xml.internal.bind.v2.TODO;

/**
 * Authentication for an application.  This authentication encorporates
 * encryption and decryption of password information.  No passwords are 
 * stored or transmitted in plain text.
 * The username/password and encryption key information is stored in a
 * simple text file.  If the file does not exist, it is created as a
 * part of the first authentication process.
 * 
 * @see AuthenticatedApplication
 * 
 * @author sgl
 *
 */
public class Authentication {

	/** The properties for the login/password pairs. */
	private static Properties props;
	/** The encryption standard used for encryption/decryption */
	private static final String AES = "AES";
	/**
	 * The name of the file containing the username/password and encryption key
	 * information.
	 */
	private static final String PWD_FILE = "login.properties";
	/** The logger for this class. */
	protected static final Logger LOGGER = LoggerFactory.getLogger();

	/**
	 * Load the login/password values from the properties file.
	 * 
	 * @return the SecretKey to be used for encryption/decryption
	 */
	private static SecretKey getKey() {
		initLoginInfo();
		String key = props.getProperty("KEY");
		SecretKey sk = null;
		// if a key is not already defined, create one and store
		// it in the login data file.
		if (key == null) {
			// TO BE IMPLEMENTED BY STUDENT
			try {
				KeyGenerator keyGen = KeyGenerator.getInstance(AES);
				keyGen.init(128); // for example
				sk = keyGen.generateKey();
                props.setProperty("KEY", Utils.byteArrayToHexString(sk.getEncoded()));
//                props.setProperty("KEY", new String(sk.getEncoded(), "UTF-8"));
                updateLoginFile();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
        } else {
            sk = new SecretKeySpec(Utils.hexStringToByteArray(key), AES);
        }
		return sk;
	}

	/**
	 * Encrypt a string
	 * 
	 * @param value
	 *            the plain text string to be encrypted.
	 * @return the hex string encryption for the given string
	 * @throws GeneralSecurityException
	 *             The encryption could not be performed.
	 */
	public static String encrypt(String value) throws GeneralSecurityException {
		// TO BE IMPLEMENTED BY STUDENT
		Cipher cipherAES = Cipher.getInstance(AES);
		cipherAES.init(Cipher.ENCRYPT_MODE, getKey(), cipherAES.getParameters());
		byte [] cipherText = cipherAES.doFinal(value.getBytes());
		return Utils.byteArrayToHexString(cipherText);
	}

	/**
	 * Decrypt a string.
	 * 
	 * @param message the hex string encryption to be decrypted
	 * @return the plain text string resulting from the decryption
	 * @throws GeneralSecurityException
	 *             invalid padding, encryption key or other decryption error was
	 *             detected.
	 */
	public static String decrypt(String message)
			throws GeneralSecurityException {
		// TO BE IMPLEMENTED BY STUDENT
        Cipher cipherAES = Cipher.getInstance(AES);
        cipherAES.init(Cipher.DECRYPT_MODE, getKey());
        byte [] cipherText = cipherAES.doFinal(message.getBytes());
        return Utils.byteArrayToHexString(cipherText);
	}

	/**
	 * Authenticate the given username and password pair
	 * 
	 * @param name
	 *            the username to be validated.
	 * @param pwd
	 *            the password specified for the given user.
	 * @return <code>true</code> if the username/password pair is validated,
	 *         <code>false</code> otherwise.
	 */
	public static boolean authenticate(String name, String pwd) throws Exception {
		initLoginInfo();
		// TO BE IMPLEMENTED BY STUDENT

		name = name.trim();
		if (name == null || name.length() == 0) {
			throw new Exception("Name cannot be null or empty");
		}

		pwd = pwd.trim();
		if (pwd == null || pwd.length() == 0) {
			throw new Exception("Password cannot be null or empty");
		}

		String registeredPwd = (String) props.get(encrypt(name));
		if (registeredPwd == null || registeredPwd.length() == 0) {
			throw new Exception("Login failed");
		}

		String encPwd;
		String encName;
		try {
			encPwd = encrypt(pwd);
			encName = encrypt(name);
		} catch (Exception e) {
			throw new Exception("Login failed");
		}

		if (!registeredPwd.equals(encPwd)) {
			throw new Exception("Login or password does not match");
		}
		return true;
	}

	/**
	 * Register the given username and password pair.  If the username
	 * given is already registered, the registration is rejected.
	 * 
	 * @param name
	 *            the username to be registered.
	 * @param pwd
	 *            the password specified for the given user.
	 * @return <code>true</code> if the username/password pair is registered,
	 *         <code>false</code> otherwise.
	 */
	public static boolean register(String name, String pwd) throws Exception {
		initLoginInfo();
		// TO BE IMPLEMENTED BY STUDENT

		name = name.trim();
		if (name == null || name.length() == 0) {
			throw new Exception("Name cannot be null or empty");
		}

		pwd = pwd.trim();
		if (pwd == null || pwd.length() == 0) {
			throw new Exception("Password cannot be null or empty");
		}

		if (props.get(name) != null) {
			throw new Exception("User with that name is already registered");
			// return false;
		}

		String encPwd;
		String encName;
		try {
			encPwd = encrypt(pwd);
			encName = encrypt(name);
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			throw new Exception("Registration failed");
			// return false;
		}
		props.put(encName, encPwd);
		return updateLoginFile();
	}

	/**
	 * De-register the given username and password pair. The username entry is
	 * removed from the list of valid
	 * 
	 * @param name
	 *            the username to be de-registered.
	 * @param pwd
	 *            the password specified for the given user.
	 * @return <code>true</code> if the username/password pair is de-registered,
	 *         <code>false</code> otherwise.
	 */
	public static boolean deregister(String name, String pwd) throws Exception {
		initLoginInfo();
		// TO BE IMPLEMENTED BY STUDENT
		if (name == null | name.length() == 0) {
			throw new Exception("Name cannot be null or empty");
		}

		if (pwd == null | pwd.length() == 0) {
			throw new Exception("Password cannot be null or empty");
		}

		if (authenticate(name, pwd)) {
			props.remove(name);
			return updateLoginFile();
		}

		return false;
	}

	/**
	 * Change the password for the given username.
	 * 
	 * @param name
	 *            the username to be modified.
	 * @param oldPwd
	 *            the existing password specified for the given user.
	 * @param newPwd
	 *            the new password to be used for the given user.
	 * @return <code>true</code> if the password is changed, <code>false</code>
	 *         otherwise.
	 */
	public static boolean changePassword(String name, String oldPwd,
			String newPwd) throws Exception {
		initLoginInfo();
		// TO BE IMPLEMENTED BY STUDENT
		if (name == null | name.length() == 0) {
			throw new Exception("Name cannot be null or empty");
		}

		if (oldPwd == null | oldPwd.length() == 0) {
			throw new Exception("Old password cannot be null or empty");
		}

		if (newPwd == null | newPwd.length() == 0) {
			throw new Exception("New password cannot be null or empty");
		}

		if (oldPwd.equals(newPwd)) {
			throw new Exception("New password cannot be equal to old password");
		}

		if (authenticate(name, oldPwd)) {
			try {
				props.setProperty(name, encrypt(newPwd));
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
				throw new Exception("Password change failed");
			}
			return updateLoginFile();
		}

		return false;
	}

	/**
	 * Update the login information file with new data.
	 * 
	 * @return <code>true</code> if the login file is updated,
	 *         <code>false</code> otherwise.
	 */
	private static boolean updateLoginFile() {
		// If there are no entries, fail the update.
		if (props == null) {
			return false;
		}
		// write out the username/password entries to a text file.
		try {
			FileWriter writer = new FileWriter(PWD_FILE);
			props.store(writer, "");
			writer.close();
		} catch (Exception e) {
			LOGGER.severe("Unable to update password file " + PWD_FILE + ": "
					+ e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Initialize the username/login information.
	 */
	private static void initLoginInfo() {
		// read in the username/password text data file.
		if (props == null) {
			props = new Properties();
			try {
				FileInputStream in = new FileInputStream(PWD_FILE);
				props.load(in);
				in.close();
			} catch (IOException e) {
				// LOGGER.warning("Could not open " + PWD_FILE + ": "
				// + e.getMessage() + ".  Creating new " + PWD_FILE);
				updateLoginFile();
			}
		}
	}

	/**
	 * Determine whether the given name is a registered username.
	 * 
	 * @param name
	 *            the name to be searched.
	 * @return <code>true</code> if the given name is a registered username,
	 *         <code>false</code> otherwise.
	 */
	public static boolean isRegisteredUsername(String name) {
		initLoginInfo();
		return props.get(name) != null;
	}

	/**
	 * A default entry point for testing.
	 * 
	 * @param arg
	 *            (not used)
	 */
	public static void main(String arg[]) {
		new Authentication();
	}
}
