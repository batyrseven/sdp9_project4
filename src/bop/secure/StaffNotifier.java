package bop.secure;

import bop.notify.NotificationServer;
import bop.notify.WaitStaffNotifier;

/**
 * An authenticated notifier for wait staff. 
 * A login layer is inserted prior to establishing a socket connection
 * to the notification server.
 * This notifier will communicate between a member of
 * the wait staff and the seating manager via the NotificationServer.
 * 
 * @see AuthenticatedApplication
 * @see NotificationServer
 * 
 * @author sgl
 * 
 */
public class StaffNotifier extends WaitStaffNotifier implements
		AuthenticatedApplication {

	/** The login user interface. */
	private AuthenticationGUI login;

	/**
	 * Default constructor. This notifier is read and write for two-way
	 * communication.
	 */
	public StaffNotifier() {
		login = new AuthenticationGUI(this);
	}

	/**
	 * Complete the login by capturing the authenticated username, closing the
	 * login frame, opening the notifier frame and running it in its own thread.
	 * 
	 * @param authenticatedUsername
	 *            the name of the user which has been authenticated by the login
	 *            process.
	 */
	public void authenticationComplete(String authenticatedUsername) {
		login.close();
		username = authenticatedUsername + NotificationServer.STAFF;
		createGUI();

		Thread thread = new Thread(this);
		thread.start();
	}

	/**
	 * Runs the client as an application.
	 * 
	 * @param args
	 *            (not used)
	 */
	public static void main(String[] args) { new StaffNotifier(); }
}
