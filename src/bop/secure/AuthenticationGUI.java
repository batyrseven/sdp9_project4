package bop.secure;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import bop.common.Globals;

/**
 * A login/authentication user interface.
 * 
 * @see AuthenticatedApplication
 * @see StaffNotifier
 * 
 * @author sgl
 * 
 */

public class AuthenticationGUI {

	/** The frame for the login window. */
	protected JFrame loginFrame;
	/** The name of this wait staff person. */
	private String username;
	/** The login frame text field for the username. */
	protected JTextField loginField;
	/** The login frame password field for the password. */
	protected JPasswordField passwordField;
	/** The application which is using this login. */
	private AuthenticatedApplication app;

	private String password;
	protected JPasswordField newPasswordField;

	/**
	 * Default constructor. This notifier is read and write for two-way
	 * communication.
	 * @param app the application which is behind the login/authentication process.
	 */
	public AuthenticationGUI(AuthenticatedApplication app) {
		this.app = app;
		createLoginGUI();
	}

	/**
	 * Create the login user interface frame.
	 */
	protected void createLoginGUI() {
		JLabel label1 = new JLabel();
		label1.setText("Username:");
		loginField = new JTextField(15);

		JLabel label2 = new JLabel();
		label2.setText("Password:");
		passwordField = new JPasswordField(15);

		newPasswordField = new JPasswordField(15);

		JButton SUBMIT = new JButton("SUBMIT");
		JButton REGISTER = new JButton("REGISTER");
		JButton UNREGISTER = new JButton("UNREGISTER");
		JButton CHANGEPASSWORD = new JButton("CHANGE PASSWORD");

		SUBMIT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String name = loginField.getText();
				String pwd = passwordField.getText();
				try {
					if (Authentication.authenticate(name, pwd)) {
						username = name;
						app.authenticationComplete(name);
					} else {
						JOptionPane.showMessageDialog(loginFrame, "Login failed.",
								"Error", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(loginFrame,
						e.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		REGISTER.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String name = loginField.getText();
				String pwd = passwordField.getText();
				try {
					if (Authentication.register(name, pwd)) {
						username = name;
						app.authenticationComplete(name);
					} else {
						JOptionPane.showMessageDialog(loginFrame,
							"Registration failed.", "Error",
							JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(loginFrame,
							e.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		UNREGISTER.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String name = loginField.getText();
				String pwd = passwordField.getText();
				try {
					if (Authentication.deregister(name, pwd)) {
						JOptionPane.showMessageDialog(loginFrame,
								"Deregistration completed.");
					} else {
						JOptionPane.showMessageDialog(loginFrame,
								"Deregistration failed.", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(loginFrame,
							e.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		CHANGEPASSWORD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String name = loginField.getText();
				String oldPwd = passwordField.getText();
				try {
					if (Authentication.authenticate(name, oldPwd)) {
						int action = JOptionPane.showConfirmDialog(null, newPasswordField, "Enter new password", JOptionPane.OK_CANCEL_OPTION);
						if (action == 0) {
							String newPwd = new String(newPasswordField.getPassword());
							if (Authentication.changePassword(name, oldPwd, newPwd)) {
								password = newPwd;
								passwordField.setText("");
								newPasswordField.setText("");
								JOptionPane.showMessageDialog(loginFrame,
										"Password change completed.");
							} else {
								JOptionPane.showMessageDialog(loginFrame,
										"Changing password failed.", "Error",
										JOptionPane.ERROR_MESSAGE);
							}
						}
					} else {
						JOptionPane.showMessageDialog(loginFrame,
								"Changing password failed.", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(loginFrame,
							e.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JPanel loginPanel = new JPanel(new GridLayout(4, 1));
		loginPanel.add(label1);
		loginPanel.add(loginField);
		loginPanel.add(label2);
		loginPanel.add(passwordField);
		loginPanel.add(SUBMIT);
		loginPanel.add(REGISTER);
		loginPanel.add(UNREGISTER);
		loginPanel.add(CHANGEPASSWORD);

		loginFrame = new JFrame();
		loginFrame.setTitle(Globals.get("BizName") + " Authentication");
		loginFrame.add(loginPanel, BorderLayout.CENTER);
		loginFrame.setSize(300, 100);
		loginFrame.pack();
		loginFrame.setVisible(true);
		loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		loginFrame.setLocationRelativeTo(null);
	}

	/**
	 * Close the authentication GUI.
	 */
	public void close() {
		loginFrame.dispose();
	}

	/**
	 * Get the authenticated user name.
	 * @return the authenticated user name, or null if the user has not been authenticated.
	 */
	public String getName() {
		return username;
	}

	/**
	 * Runs the client as an application.
	 * 
	 * @param args
	 *            (not used)
	 */
	public static void main(String[] args) {
		class TestApp implements AuthenticatedApplication {
			public TestApp() {
			}

			public void authenticationComplete(String username) {
			}
		}
		AuthenticatedApplication app = new TestApp();
		new AuthenticationGUI(app);
	}
}
