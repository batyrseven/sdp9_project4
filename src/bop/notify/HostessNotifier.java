package bop.notify;

import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import bop.seating.SeatingManager;
import bop.seating.SeatingObserver;

/**
 * A notifier for the hostess/seating manager. This notifier sends system alerts
 * to customers.
 * 
 * @see NotificationClient
 * 
 * @author sgl
 * 
 */
public class HostessNotifier extends NotificationClient implements Runnable,
		SeatingObserver {

	/**
	 * Default constructor. This notifier sends system messages, so no GUI is
	 * necessary.
	 */
	public HostessNotifier() {
		username = NotificationServer.HOSTESS;
		SeatingManager.registerObserver(this);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		createTextEntryField();
		createMessageArea();
		new Thread(this).start();
	}

	/**
	 * Notify the given party that they will be seated at the specified table.
	 * This implements the SeatingObserver.
	 * 
	 * @param party
	 *            the name of the party to be seated.
	 * @param tableNumber
	 *            the number of the table at which the party will be seated.
	 */
	public void seat(String party, int tableNumber) {
		// notify the party that they are ready to be seated.
		sendPrivateMessage(
				"You are ready to be seated at Table " + tableNumber, party);
	}

	/**
	 * Thank the given party and terminate their notifier.
	 * This implements the SeatingObserver.
	 * 
	 * @param party
	 *            the name of the party to be seated.
	 * @param tableNumber
	 *            the number of the table at which the party will be seated.
	 */
	public void clear(String party, int tableNumber) {
		// attempt to send a thank you to the notification client for this
		// party.
		sendPrivateMessage("Thank you for dining at " + BIZNAME + "!", party);

		out.println("TO Hostess Cleared " + username + " at table "
				+ tableNumber);
		// terminate the notification client for this party.
		sendTermination(party, 10);
	}

	/**
	 * Notify the given party that they have been added to the wait list at the
	 * specified position.
	 * This implements the SeatingObserver.
	 * 
	 * @param party
	 *            the name of the party to be seated.
	 * @param position
	 *            the position in the wait list where the party is listed.
	 */
	public void addToWaitList(String party, int position) {
		// attempt to send a message to the notification client for this party
		// telling them what position they are in the list.
		sendPrivateMessage("You have been added to the Wait List as #"
				+ position, party);
	}

	/**
	 * This implements the SeatingObserver.
	 * (No notifications are sent when removing a party from the wait list)
	 */
	public void removeFromWaitList(String party) {
	}

	/**
	 * Send a message to all customers on the wait list notifying them of their
	 * updated position in the wait list.
	 * This implements the SeatingObserver.
	 */
	public void updateWaitList(Vector<String> waitlist, int index) {
		sendUpdateMessages(waitlist, index);
	}

	/**
	 * Get the notifier user interface panel. This panel is inserted into the
	 * Seating Manager user interface.
	 * 
	 * @return the panel for this hostess notifier client.
	 */
	public JPanel getPanel() {
		return panel;
	}

	/**
	 * Send a message to a specific notification client.
	 * 
	 * @param msg
	 *            the message to send.
	 * @param toName
	 *            the name of the client to send the message to.
	 */
	public void sendPrivateMessage(String msg, String toName) {
		out.println("TO " + toName + " " + msg);
	}

	/**
	 * Send a message to all notification clients.
	 * 
	 * @param msg
	 *            the message to send.
	 */
	public void sendBroadcastMessage(String msg) {
		out.println(msg);
	}

	/**
	 * Terminate a given client.
	 * 
	 * @param clientName
	 *            the name of the client to terminate.
	 * @param time
	 *            the time to display the termination message before actually
	 *            closing the client.
	 */
	public void sendTermination(String clientName, int time) {
		out.println("TERMINATE " + clientName + " " + time);
	}

	/**
	 * Send a waitlist update message to registered customer clients. Only send
	 * messages to customers from specified index to the end of the list. This
	 * is so that if a customer is removed from the list, only the customers
	 * that were later in the list are bumped up and notified. Customers earlier
	 * in the list are not affected and need no notification.
	 * 
	 * @param waitlist
	 *            the current wait list in the form of an ordered vector of
	 *            names.
	 * @param index
	 *            the index in the wait list from which to begin notifications
	 */
	public void sendUpdateMessages(Vector<String> waitlist, int index) {
		int waitlistSize = waitlist.size();
		if (index > waitlistSize || index < 1) {
			index = 1;
		}
		// for each name in the waitlist, attempt to send an update message
		for (int i = waitlistSize; i >= index; i--) {
			int position = waitlistSize - 1 + 1;
			sendPrivateMessage("You are #" + position + " on the waitlist.",
					waitlist.get(i - 1));
		}
	}

	/**
	 * Runs the client as an application with a closeable frame.
	 * 
	 * @param args
	 *            (not used)
	 */
	public static void main(String[] args) throws Exception {
		HostessNotifier client = new HostessNotifier();
		client.createGUI();
		client.run();
	}
}
