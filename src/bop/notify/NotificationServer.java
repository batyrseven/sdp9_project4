package bop.notify;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.logging.Logger;

import bop.common.Globals;
import bop.common.LoggerFactory;
import bop.seating.SeatingChart;
import bop.seating.SeatingManager;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

/**
 * A multi-threaded notification server. This server maintains a list of all
 * registered notification clients. Each client must submit a unique name to be
 * added to the list.
 * <p>
 * 
  * This server has the following protocol:
 * <p>
 * - Upon initial connection the server sends "<b>SUBMITNAME</b>."
 * <p>
 * - The client replies with the desired screen name.i.	
 * Staff clients will append � (Staff)� to the name given before registering it with the server.  
 * This will identify the client as a staff client. 
 * Customer clients will register the name as given by the user with the client.
 * <p>
 * - The server will keep sending "<b>SUBMITNAME</b>" requests until the client
 * submits a screen name that is not already in use.
 * <p>
 * - The server sends a line beginning with "<b>NAMEACCEPTED</b>" when the
 * client submits a screen name that is not already in use.
 * <p>
 * - The client will then receive messages sent to it from the server. Messages
 * will begin with "<b>MESSAGE </b>". If the client is visible, all of the
 * characters following "<b>MESSAGE </b>" are displayed in the text area of the
 * GUI.
 * <p>
 * - If the client is not read-only, the client may send messages to the server.
 * These messages are entered in the text field in the GUI. The client prepends
 * the message with "<b>MESSAGE </b>" and sends it to the server for delivery.
 * If the message is a private message to be delivered only to a specific client 
 * the �TO {client name}� prepends the MESSAGE instruction (i.e. �TO Hostess MESSAGE Checking in�).
 * Messages without the TO directive will be broadcast to all clients.
 * <p>
 * - If the client sends a "<b>CLEAR</b>" request the server will have the 
 * seating manager clear the table and then notify the staff.
 * <p>
 * - If the server sends a "<b>TERMINATE</b>" request the client will alert the
 * user and then close.
 * 
 * @see NotificationClient
 * 
 * @author sgl
 */
/*
 * INSTRUCTOR NOTES:
 * As scaffolding projects for more advanced students, students might:
 * - validate that only a single connection is made from any given client machine.
 * As implemented, multiple notifiers can be created from a single device.  
 * As a scaffolding exercise, add the restriction that only a single connection
 * can be made from any given client device. 
 */
public class NotificationServer extends Thread {

	/** The name of the server */
	protected static final String BIZNAME = Globals.get("BizName");
	/** The port that the server listens on. */
	protected static final String PORT = Globals.get("NotificationServerPort");
	/** The name of the seating manager hostess client notifier. */
	public final static String HOSTESS = "Hostess";
	/** The staff designator for the seating manager client notifiers. */
	public final static String STAFF = " (Staff)";
	/** The logger for this class. */
	protected static final Logger LOGGER = LoggerFactory.getLogger();
	/** The set of all the print writers for all the clients. */
	protected static Hashtable<String, PrintWriter> writers = new Hashtable<String, PrintWriter>();

	/**
	 * Default constructor. This creates and starts the notification server.
	 */
	public NotificationServer() {
	}

	/**
	 * Run this server in its own thread.
	 */
	@Override
	public void run() {
		try {
			System.setProperty("javax.net.ssl.keyStore", "BOPKeysotre.jks");
			System.setProperty("javax.net.ssl.keyStorePassword", "bigoven");
			System.setProperty("javax.net.ssl.trustStore", "BOPTruststore");
			System.setProperty("javax.net.ssl.trustStorePassword", "bigoven");
			System.setProperty("javax.net.debug", "ssl");

			SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
//			ServerSocket serverSocket = new ServerSocket(Integer.parseInt(PORT));
			SSLServerSocket serverSocket = (SSLServerSocket) ssf.createServerSocket(Integer.parseInt(PORT));

			LOGGER.info("\n*******************************************************\nThe "
					+ BIZNAME
					+ " notification server is running on port "
					+ PORT + ".");
			while (true) {
				Handler handler = new Handler((SSLSocket) serverSocket.accept());
				handler.start();
			}
		} catch (IOException ioe) {
			System.err
					.println("Unable to start NotificationServer: Server socket was closed.");
			System.err.println(ioe.getMessage());
		}
	}

	/**
	 * A notification handler class threaded for each client. Handlers are spawned from the listening loop and
	 * are responsible for a dealing with a single client and broadcasting its
	 * messages.
	 */
	public static class Handler extends Thread {
		/*** The socket related to this handler. */
		private SSLSocket socket;

		/**
		 * Constructs a handler thread, storing the socket.
		 * @param socket the socket associated with this handler.
		 */
		public Handler(SSLSocket socket) {
			this.socket = socket;
		}

		/**
		 * Repeatedly request a screen name until a unique one has been
		 * submitted. Then acknowledge the name and register the output stream
		 * for the client in a global set. Then repeatedly get messages and
		 * handle them according to the protocol.
		 */
		@Override
		public void run() {
			String fromName = "";
			BufferedReader in = null;
			PrintWriter out = null;
			boolean staff = false;
			try {
				InetAddress address = socket.getInetAddress();
				String ipAddr = address.getHostAddress();
				String[] ipBits = ipAddr.split("\\.");
				if (!(ipBits[0].equals("192") || address.isSiteLocalAddress() || // client
																				// on
																				// server
						ipBits[0].equals("127") || address.isLoopbackAddress())) { // client
																					// on
																					// LAN
					LOGGER.warning("Notification request from WAN rejected: " + ipAddr);
					return;
					
				}

				// Create input/output streams for the socket.
				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);

				// Request a name from this client.
				// Keep requesting until a unique name is submitted.
				// Note that the set of names must be locked while checking and
				// adding names since this is a multi-threaded server.
				while (true) {
					out.println("SUBMITNAME");
					fromName = in.readLine();
					if (fromName == null) {
						return;
					}
					if (fromName.equals(HOSTESS)) {
						staff = true;
					}
					/* Project 3 */
					if (fromName.endsWith(STAFF)) {
						fromName = "*"
								+ fromName.substring(0, fromName.length() - 8)
								+ "*";
						staff = true;
					}

					// Once a unique name is submitted, add the
					// socket's print writer to the list of all clients.
					synchronized (writers) {
						if (staff
								|| (fromName.length() > 0
										&& !writers.keySet().contains(fromName) && !SeatingManager
											.isWaiting(fromName))) {
							writers.put(fromName, out);
							LOGGER.info(fromName + ":\tCONNECTED from "
									+ ipAddr);
							out.println("NAMEACCEPTED " + fromName);
							if (staff) {
								sendPrivateMessage(fromName + " checking in.",
										fromName, HOSTESS);
							}
							break;
						} else {
							out.println("NAMEREJECTED " + fromName);
						}
					}
				}
				// if the client is not the hostess client, add the client's
				// name to the waiting list
				if (!staff) {
					SeatingManager.addToWaitList(fromName);
				}

				// Accept messages from this client and handle them.
				while (true) {
					String input = "";
					try {
						input = in.readLine();
					} catch (SocketException se) {
						LOGGER.info(fromName + ":\tDISCONNECTED");
						PrintWriter writer = writers.get(fromName);
						if (writer != null) {
							writer.close();
						}
						writers.remove(fromName);
						input = null;
					}
					if (input == null || input.length() == 0) {
						return;
					}
					// direct a termination directive to this client
					if (input.toUpperCase().startsWith("TERMINATE")) {
						String toName = input.substring(10);
						String time = toName.substring(toName.indexOf(" ") + 1);
						toName = toName.substring(0, toName.indexOf(" "));
						// do not allow anyone to terminate the hostess
						// notifier.
						if (!toName.equals(HOSTESS)) {
							sendTermination(fromName, toName,
									Integer.parseInt(time));
						}
						// direct a private message to the specified client.
					} else if (input.toUpperCase().startsWith("TO ")) {
						String toName = input.substring(3);
						String msg = toName.substring(toName.indexOf(" ") + 1);
						toName = toName.substring(0, toName.indexOf(" "));
						sendPrivateMessage(msg, fromName, toName);
						// clear a table remotely
					} else if (input.toUpperCase().startsWith("CLEAR ")) {
						String table = input.substring(6);
						String party = SeatingChart.getPartyAt(Integer
								.parseInt(table));
						SeatingManager.clear(party, Integer.parseInt(table));
						sendStaffMessage("Table " + table + " cleared.",
								fromName);
						// direct a broadcast message to all clients
					} else {
						sendBroadcastMessage(input, fromName);
					}
				}
			} catch (IOException e) {
				LOGGER.severe("Handler error: " + e.getMessage());
			} finally {
				// This client has terminated.
				// Remove its name from the list of client and close its socket.
				if (out != null) {
					writers.remove(out);
				}
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}

		/**
		 * Send a termination directive to a client.
		 * 
		 * @param fromName
		 *            the name of the client initiating the termination.
		 * @param toName
		 *            the name of the client to be terminated.
		 * @param time
		 *            the duration in seconds to display a termination message
		 *            before actually closing the client.
		 */
		public static void sendTermination(String fromName, String toName,
				int time) {
			Iterator<String> iter = writers.keySet().iterator();
			while (iter.hasNext()) {
				String clientname = iter.next();
				if (clientname.equals(toName)) {
					PrintWriter writer = writers.get(toName);
					writer.println("TERMINATE " + time * 1000);
					LOGGER.info(fromName + ":\tTERMINATE " + toName);
					return;
				}
			}
		}

		/**
		 * Send a private message from one client to another.
		 * 
		 * @param input
		 *            the message to send.
		 * @param fromName
		 *            the client from which the message originated.
		 * @param toName
		 *            the client to which the message is directed.
		 */
		public static void sendPrivateMessage(String input, String fromName,
				String toName) {
			// ignore messages sent to yourself.
			if (fromName.equals(toName)) {
				return;
			}
			Iterator<String> iter = writers.keySet().iterator();
			while (iter.hasNext()) {
				String clientname = iter.next();
				if (clientname.equals(toName) || clientname.equals(HOSTESS)) {
					PrintWriter writer = writers.get(toName);
					if (writer == null) {
						return;
					}
					String msg = "MESSAGE " + fromName + " to " + toName + ": "
							+ input;
					writer.println(msg);
					LOGGER.info(msg);
					// echo message back to sender
					writer = writers.get(fromName);
					writer.println(msg);
					return;
				}
			}
		}

		/**
		 * Send a broadcast message to all clients.
		 * 
		 * @param input
		 *            the message to be broadcast.
		 * @param fromName
		 *            the client from which the message originated.
		 */
		public static void sendBroadcastMessage(String input, String fromName) {
			LOGGER.info(fromName + ": " + input);
			Iterator<PrintWriter> iter = writers.values().iterator();
			while (iter.hasNext()) {
				PrintWriter writer = iter.next();
				writer.println("MESSAGE " + fromName + ": " + input);
			}
		}

		/**
		 * Send a broadcast message to all clients.
		 * 
		 * @param input
		 *            the message to be broadcast.
		 * @param fromName
		 *            the client from which the message originated.
		 */
		public static void sendStaffMessage(String input, String fromName) {
			Iterator<String> iter = writers.keySet().iterator();
			while (iter.hasNext()) {
				String clientname = iter.next();
				if ((clientname.startsWith("*") && clientname.endsWith("*"))
						|| clientname.equals(HOSTESS)) {
					PrintWriter writer = writers.get(clientname);
					if (writer == null) {
						return;
					}
					String msg = "MESSAGE " + fromName + " to " + clientname
							+ ": " + input;
					writer.println(msg);
					return;
				}
			}
		}
	}

	/**
	 * The application main method, which just listens on a port and spawns
	 * handler threads.
	 * 
	 * @param args
	 *            (not used)
	 */
	public static void main(String[] args) {
		NotificationServer server = new NotificationServer();
		server.run();
	}
}
