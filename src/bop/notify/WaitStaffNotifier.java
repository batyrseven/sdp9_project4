package bop.notify;

import javax.swing.JOptionPane;

/**
 * A notifier for wait staff. This notifier will communicate between a member of
 * the wait staff and the seating manager.
 * 
 * @see NotificationServer
 * @see NotificationClient
 * 
 * @author sgl
 * 
 */
public class WaitStaffNotifier extends NotificationClient {

	/**
	 * Default constructor. This notifier is read and write for two-way
	 * communication.
	 */
	public WaitStaffNotifier() {
	}

	/**
	 * Send a message.
	 * Messages are sent to the hostess only by default.
	 */
	@Override
	protected void sendMessage(String msg) {
		// default to send message to Hostess only
		if (!msg.toUpperCase().startsWith("TO ")
				&& !msg.toUpperCase().startsWith("CLEAR ")) {
			out.println("TO Hostess " + msg);
		} else {
			out.println(msg);
		}
	}

	/**
	 * Prompt for and return the desired screen name.
	 * 
	 * @return the name of this client.
	 */
	@Override
	protected String getClientName() {
		String name = JOptionPane.showInputDialog(frame,
				"Please enter your name:", BIZNAME + " Wait List Add",
				JOptionPane.PLAIN_MESSAGE);
		if (name != null && name.length() > 0) {
			return name += NotificationServer.STAFF;
		}
		return null;
	}

	/**
	 * Runs the client as an application.
	 * 
	 * @param args
	 *            (not used)
	 */
	public static void main(String[] args) {
		WaitStaffNotifier client = new WaitStaffNotifier();
		client.username = "user";
		client.createGUI();
		client.run();
	}
}
