package bop.notify;

/**
 * A notifier for customers. This notifier will alert a customer to messages
 * from the seating manager application.
 * 
 * @see NotificationClient
 * 
 * @author sgl
 * 
 */
public class CustomerNotifier extends NotificationClient {

	/**
	 * Default constructor. Customers only receive messages. They do not
	 * generate them.
	 */
	public CustomerNotifier() {
	}

	/**
	 * Overrides text entry field creation, leaving this out of the notification
	 * panel and making this client read-only.
	 */
	@Override
	public void createTextEntryField() {
	}

	/**
	 * Runs the client.
	 * 
	 * @param args
	 *            (not used).
	 */
	public static void main(String[] args) throws Exception {
		CustomerNotifier client = new CustomerNotifier();
		client.createGUI();
		client.run();
	}
}
