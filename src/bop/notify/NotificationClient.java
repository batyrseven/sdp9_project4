package bop.notify;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import bop.common.Globals;

/**
 * A generic notification client. This client may be read-only (only receives
 * messages and does not send them) or read-write (sends and receives messages).
 * This client may be visible (for user interaction) or not (for system
 * interaction).
 * 
  * This server has the following protocol:
 * <p>
 * - Upon initial connection the server sends "<b>SUBMITNAME</b>."
 * <p>
 * - The client replies with the desired screen name.i.	
 * Staff clients will append � (Staff)� to the name given before registering it with the server.  
 * This will identify the client as a staff client. 
 * Customer clients will register the name as given by the user with the client.
 * <p>
 * - The server will keep sending "<b>SUBMITNAME</b>" requests until the client
 * submits a screen name that is not already in use.
 * <p>
 * - The server sends a line beginning with "<b>NAMEACCEPTED</b>" when the
 * client submits a screen name that is not already in use.
 * <p>
 * - The client will then receive messages sent to it from the server. Messages
 * will begin with "<b>MESSAGE </b>". If the client is visible, all of the
 * characters following "<b>MESSAGE </b>" are displayed in the text area of the
 * GUI.
 * <p>
 * - If the client is not read-only, the client may send messages to the server.
 * These messages are entered in the text field in the GUI. The client prepends
 * the message with "<b>MESSAGE </b>" and sends it to the server for delivery.
 * If the message is a private message to be delivered only to a specific client 
 * the �TO {client name}� prepends the MESSAGE instruction (i.e. �TO Hostess MESSAGE Checking in�).
 * Messages without the TO directive will be broadcast to all clients.
 * <p>
 * - If the client sends a "<b>CLEAR</b>" request the server will have the 
 * seating manager clear the table and then notify the staff.
 * <p>
 * - If the server sends a "<b>TERMINATE</b>" request the client will alert the
 * user and then close.
 * 
 * @see NotificationServer
 * 
 * @author sgl
 */
/* 
 * As scaffolding projects for more advanced students, students might:
 * - turn this into an applet to run embedded in a website
 */
public class NotificationClient implements Runnable {

	/** The name of the business to display. */
	protected static final String BIZNAME = Globals.get("BizName");
	/** The port on the notification server to be used for notifications. */
	protected static final String PORT = Globals.get("NotificationServerPort");
	/** The notification server. */
	protected static final String SERVER = Globals.get("ServerIP");
	/** The user name for this client. */
	protected String username = null;
	/** The input reader. */
	protected BufferedReader in;
	/** The output writer. */
	protected PrintWriter out;
	
	/** The GUI frame for the client. */
	protected JFrame frame = new JFrame("Notification Client");
	/** The panel containing the text entry and message areas. */
	protected JPanel panel = new JPanel();
	/** The message entry text field. */
	protected JTextField textField = new JTextField(40);
	/** The message display text area. */
	protected JTextArea messageArea = new JTextArea(8, 40);
	/** The scrolling pane for the text message area. */
	protected JScrollPane scrollMessageArea;

	/**
	 * Default constructor for the notification client.
	 */
	public NotificationClient() {
	}

	/**
	 * Create the user interface for the notification client.
	 */
	protected void createGUI() {
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		createTextEntryField();
		createMessageArea();
		frame.add(panel);
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Create the text entry field in the notification client.
	 */
	public void createTextEntryField() {
		// The textfield is not editable until the client has
		// successfully registered with the server.
		textField.setEditable(false);
		panel.add(textField);

		// Add listener for message entry in the textfield
		textField.addActionListener(new ActionListener() {
			/**
			 * Responds to pressing the enter key in the textfield by sending
			 * the contents of the text field to the server. Then clear the text
			 * area in preparation for the next message.
			 */
			public void actionPerformed(ActionEvent e) {
				sendMessage(textField.getText());
				textField.setText("");
			}
		});
	}

	/**
	 * Create the message display area in the notification client.
	 */
	public void createMessageArea() {
		messageArea.setEditable(false);
		scrollMessageArea = new JScrollPane(messageArea);
		// always scroll to the latest messages/bottom of the pane
		scrollMessageArea.getVerticalScrollBar().addAdjustmentListener(
				new AdjustmentListener() {
					public void adjustmentValueChanged(AdjustmentEvent e) {
						e.getAdjustable().setValue(
								e.getAdjustable().getMaximum());
					}
				});
		panel.add(scrollMessageArea);
	}

	/**
	 * Send a notification message.
	 * @param msg the message to be sent.
	 */
	protected void sendMessage(String msg) {
		out.println(msg);
	}

	/**
	 * Prompt for and return the desired screen name.
	 * 
	 * @return the name of this client.
	 */
	protected String getClientName() {
		return JOptionPane.showInputDialog(frame, "Please enter your name:",
				BIZNAME + " Wait List Add", JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * Connects to the server then enters the processing loop.
	 */
	public void run() {
		try {
			System.setProperty("javax.net.ssl.keyStore", "BOPKeysotre.jks");
			System.setProperty("javax.net.ssl.keyStorePassword", "bigoven");
			System.setProperty("javax.net.ssl.trustStore", "BOPTruststore");
			System.setProperty("javax.net.ssl.trustStorePassword", "bigoven");
			System.setProperty("javax.net.debug", "ssl");

			// Make connection and initialize the input/output streams
			SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
			SSLSocket socket = (SSLSocket)sslsocketfactory.createSocket(SERVER, Integer.parseInt(PORT));
//			Socket socket = new Socket(SERVER, Integer.parseInt(PORT));

			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);

			System.out.println("Run");

			// Process all messages from server, according to the protocol.
			while (true) {
				handleRequests();
			}
			// if no server is reachable, alert the user and then terminate.
		} catch (IOException ioe) {
			messageArea
					.append(BIZNAME + " is not seating tables at this time.");
			System.err.println("Notification Client Socket was not created: "
					+ ioe.getMessage());
			try {
				Thread.sleep(10000);
			} catch (InterruptedException ie) {
			}
			System.exit(0);
		}
	}

	/**
	 * Handle all requests of this client.
	 * @throws IOException the request could not be processed.
	 */
	public void handleRequests() throws IOException {
		System.out.println("\nHandling new request");

		// if we don't have a name for this client yet, prompt for one.
		if (username == null) {
			username = getClientName();
		}
		// Cancel
		if (username == null) {
			System.exit(0);
		}

		System.out.println("Username: " + username);

		String line = "";
		// read a line of input
		try {
			line = in.readLine();
		} catch (SocketException se) {
			System.err.println("Socket ERROR: Unable to handle request: "
					+ se.getMessage());
			System.exit(1);
		}

		System.out.println("Received: " + line);

		// register the client name with the server
		if (line.toUpperCase().startsWith("SUBMITNAME")) {
			out.println(username);
			// the registration request has been rejected.
		} else if (line.startsWith("NAMEREJECTED")) {
			String name = line.substring(13);
			username = null;
			JOptionPane.showConfirmDialog(frame, "The name " + name
					+ " is already in use.  Please give a different name.",
					BIZNAME + " Wait List Add", JOptionPane.PLAIN_MESSAGE);
			// the registration request has been accepted.
		} else if (line.toUpperCase().startsWith("NAMEACCEPTED")) {
			String name = line.substring(13);
			frame.setTitle(name);
			textField.setEditable(true);
			// display the given message
		} else if (line.toUpperCase().startsWith("MESSAGE")) {
			messageArea.append(line.substring(8) + "\n");
			// terminate this client
		} else if (line.toUpperCase().startsWith("TERMINATE")) {
			String time = line.substring(10);
			try {
				Thread.sleep(Long.parseLong(time));
			} catch (InterruptedException ie) {
			}
			System.exit(0);
		}
	}

	/**
	 * Runs the client as an application with a closeable frame.
	 * 
	 * @param args
	 *            (not used)
	 */
	public static void main(String[] args) throws Exception {
		NotificationClient client = new NotificationClient();
		client.createGUI();
		client.run();
	}
}
