package bop.seating;

import java.awt.GridBagLayout;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * The seating chart panel of the seating manager. This class handles
 * drag-and-drop and mouse click events to display the current state of customer
 * seating.  The seating chart represents the physical location of the
 * tables and service areas in the restaurant.
 * 
 * @see Table
 * @see ServiceArea
 * 
 * @author sgl
 * 
 */
public class SeatingChart extends JPanel {

	/** The serial UID */
	private static final long serialVersionUID = 1L;
	/** The seating chart frame. */
	static JFrame frame;
	/** The layout for the seating chart frame. */
	static GridBagLayout layout = new GridBagLayout();

	/** The tables which are tracked in the seating chart. */
	private static Table table01, table02, table03, table04, table05, table06,
			table07, table08, table09, table10, table11, table12, table13,
			table14, table15, table16, table17, table18, table19, table20,
			table21, table22, table23, table24, table25, table26, table27,
			table28, table29, table30, table31, table32, table33, table34,
			table35, table36, table37, table38, table39, table40, table41,
			table42, table43, table44, table45, table46, table47, table48,
			table49, table50, table51, table52, table53;

	/** The vector of tables which are represented in this seating chart. */
	private static Vector<Table> tables = new Vector<Table>();

	/**
	 * Default constructor. This constructor creates the seating chart with a
	 * specific arrangement and number of tables.
	 */
	public SeatingChart() {
		super(layout);
		setBorder(BorderFactory.createTitledBorder("Seating Chart"));

		// create and place each table in the restaurant layout
		table01 = new Table(1, layout, 0, 0, this);
		table02 = new Table(2, layout, 0, 1, this);
		table03 = new Table(3, layout, 0, 2, this);
		table04 = new Table(4, layout, 0, 3, this);
		table05 = new Table(5, layout, 0, 4, this);
		table06 = new Table(6, layout, 0, 5, this);
		table07 = new Table(7, layout, 0, 6, this);
		table08 = new Table(8, layout, 0, 7, this);
		table09 = new Table(9, layout, 0, 8, this);
		table10 = new Table(10, layout, 0, 9, this);
		table11 = new Table(11, layout, 1, 0, this);
		table12 = new Table(12, layout, 2, 0, this);
		table13 = new Table(13, layout, 2, 2, this);
		table14 = new Table(14, layout, 2, 3, this);
		table15 = new Table(15, layout, 2, 4, this);
		table16 = new Table(16, layout, 2, 5, this);
		table17 = new Table(17, layout, 2, 6, this);
		table18 = new Table(18, layout, 2, 7, this);
		table19 = new Table(19, layout, 2, 8, this);
		table20 = new Table(20, layout, 3, 0, this);
		table21 = new Table(21, layout, 3, 2, this);
		table22 = new Table(22, layout, 3, 3, this);
		table23 = new Table(23, layout, 3, 4, this);
		table24 = new Table(24, layout, 3, 5, this);
		table25 = new Table(25, layout, 3, 6, this);
		table26 = new Table(26, layout, 3, 7, this);
		table27 = new Table(27, layout, 3, 8, this);
		table28 = new Table(28, layout, 4, 0, this);
		table29 = new Table(29, layout, 5, 0, this);
		table30 = new Table(30, layout, 5, 2, this);
		table31 = new Table(31, layout, 5, 4, this);
		table32 = new Table(32, layout, 5, 6, this);
		table33 = new Table(33, layout, 5, 8, this);
		table34 = new Table(34, layout, 6, 0, this);
		table35 = new Table(35, layout, 7, 0, this);
		table36 = new Table(36, layout, 7, 2, this);
		table37 = new Table(37, layout, 7, 4, this);
		table38 = new Table(38, layout, 7, 6, this);
		table39 = new Table(39, layout, 7, 8, this);
		table40 = new Table(40, layout, 12, 0, this);
		table41 = new Table(41, layout, 12, 2, this);
		table42 = new Table(42, layout, 12, 4, this);
		table43 = new Table(43, layout, 13, 0, this);
		table44 = new Table(44, layout, 14, 0, this);
		table45 = new Table(45, layout, 14, 2, this);
		table46 = new Table(46, layout, 14, 4, this);
		table47 = new Table(47, layout, 15, 0, this);
		table48 = new Table(48, layout, 16, 0, this);
		table49 = new Table(49, layout, 16, 1, this);
		table50 = new Table(50, layout, 16, 2, this);
		table51 = new Table(51, layout, 16, 3, this);
		table52 = new Table(52, layout, 16, 4, this);
		table53 = new Table(53, layout, 16, 5, this);
		new ServiceArea("Plant", layout, 8, 0, 1, 1, this);
		new ServiceArea("Plant", layout, 11, 0, 1, 1, this);
		new ServiceArea("Hostess", layout, 9, 2, 2, 2, this);
		new ServiceArea("Bar", layout, 9, 6, 6, 8, this);

		tables.add(table01);
		tables.add(table02);
		tables.add(table03);
		tables.add(table04);
		tables.add(table05);
		tables.add(table06);
		tables.add(table07);
		tables.add(table08);
		tables.add(table09);
		tables.add(table10);
		tables.add(table11);
		tables.add(table12);
		tables.add(table13);
		tables.add(table14);
		tables.add(table15);
		tables.add(table16);
		tables.add(table17);
		tables.add(table18);
		tables.add(table19);
		tables.add(table20);
		tables.add(table21);
		tables.add(table22);
		tables.add(table23);
		tables.add(table24);
		tables.add(table25);
		tables.add(table26);
		tables.add(table27);
		tables.add(table28);
		tables.add(table29);
		tables.add(table30);
		tables.add(table31);
		tables.add(table32);
		tables.add(table33);
		tables.add(table34);
		tables.add(table35);
		tables.add(table36);
		tables.add(table37);
		tables.add(table38);
		tables.add(table39);
		tables.add(table40);
		tables.add(table41);
		tables.add(table42);
		tables.add(table43);
		tables.add(table44);
		tables.add(table45);
		tables.add(table46);
		tables.add(table47);
		tables.add(table48);
		tables.add(table49);
		tables.add(table50);
		tables.add(table51);
		tables.add(table52);
		tables.add(table53);
	}

	/**
	 * Perform an action based on the given event.
	 * 
	 * @param e
	 *            (not used)
	 */
	public void actionPerformed(ActionEvent e) {
	}

	/**
	 * Get the party at the specified table.
	 * @param tableNumber the table number which is being queried.
	 * @return the name of the party seated at the specified table.
	 */
	public static String getPartyAt(int tableNumber) {
		if (tableNumber > 0 && tableNumber < tables.size()) {
			return tables.get(tableNumber - 1).getPartyName();
		} else {
			return "UNKNOWN";
		}
	}

	/**
	 * Enable drag-and-drop drop targeting for tables.
	 * 
	 * @param enabled <code>true</code> if drag behavior is enabled, <code>false</code> otherwise.
	 */
	public void setDragEnabled(boolean enabled) {
		DropTarget target = new DropTarget();
		table01.setDropTarget(target);
	}

	/**
	 * Create and show the seating chart panel.
	 */
	private static void createAndShowGUI() {
		// Create and set up the window.
		frame = new JFrame("Seating Chart");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		JComponent newContentPane = new SeatingChart();
		frame.add(newContentPane);
		newContentPane.setOpaque(true); // content panes must be opaque
		frame.setContentPane(newContentPane);

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Main entry point to test the seating chart in a stand alone mode.
	 * 
	 * @param args
	 *            (not used)
	 */
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
