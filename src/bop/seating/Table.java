package bop.seating;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import bop.common.LoggerFactory;

/**
 * A button representing a table.  These buttons
 * are then oriented in the seating chart.  
 * This class implements the SeatingObserver to handle seating events.
 * 
 * @see SeatingChart
 * @see SeatingObserver
 * 
 * @author sgl
 * 
 */
@SuppressWarnings("serial")
class Table extends JButton implements MouseListener, SeatingObserver {

	/** The logger to be used for this class. */
	private final static Logger LOGGER = LoggerFactory.getLogger();
	/** A string indicating that the table is open. */
	static final String OPEN = "OPEN";
	/** A string indicating that the table is reserved. */
	static final String RESERVED = "Reserved";
	/** A string indicating that the table has been seated. */
	static final String SEATED = "Seated";
	/** The size of a table button. */
	static final int TABLESIZE = 40;
	/** The name of a party of customers seated at this table. */
	private String partyName;
	/** The table number designation. */
	private int tableNumber;
	/** The current state (ie. open, reserved, seated) of this table. */
	private String state;

	/**
	 * Constructor.
	 * 
	 * @param tableNo
	 *            the table number designation for this table.
	 * @param layout
	 *            the layout in which to place this table.
	 * @param x
	 *            the x coordinate at which to place this table in the layout.
	 * @param y
	 *            the y coordinate at which to place this table in the layout.
	 * @param pane
	 *            the pane in which to place this table button.
	 */
	public Table(int tableNo, GridBagLayout layout, int x, int y, Container pane) {
		super();
		SeatingManager.registerObserver(this);
		tableNumber = tableNo;
		init();
		setMargin(new Insets(1, 1, 1, 1));
		// set up drag-and-drop drop behavior
		addMouseListener(this);
		new DropTarget(this, new DropBehavior(this));
		// layout GUI
		setPreferredSize(new Dimension(TABLESIZE, TABLESIZE));
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.fill = GridBagConstraints.BOTH;
		layout.setConstraints(this, constraints);
		pane.add(this);
	}

	/**
	 * Get the name of the party at this table.
	 * @return the name of the party at this table.  If no party
	 * is currently seated, this will return OPEN.
	 */
	public String getPartyName() {
		return partyName;
	}

	/**
	 * Handle a mouse-clicked event for a table.
	 * 
	 * @param e
	 *            the mouse event to handle.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {

		if (e.getButton() == MouseEvent.BUTTON1) {
			if (state.equals(OPEN)) {
				String party = SeatingManager.getNextParty();
				if (party != null) {
					SeatingManager.seat(party, tableNumber);
				}
			} else {
				SeatingManager.clear(partyName, tableNumber);
			}
		} else if (e.getButton() == MouseEvent.BUTTON3) {
			if (state.equals(OPEN)) {
				reserve(askForReservationName());
			} else if (state.equals(RESERVED)) {
				SeatingManager.seat(partyName, tableNumber);
			}
		}
	}

	/**
	 * Reserve this table.
	 * 
	 * @param party
	 *            the name of the party under which the reservation is to be
	 *            made.
	 */
	public void reserve(String party) {
		if (party == null || party.length() == 0)
			return;
		partyName = party;
		setText("<html><center>" + tableNumber + "<br>" + party
				+ "</center></html>");
		setBackground(Color.GRAY);
		setFont(new Font("Arial", Font.PLAIN, 12));
		getModel().setPressed(false);
		state = RESERVED;
		LOGGER.info("Reserved table " + tableNumber + " for " + party);
	}

	/**
	 * Seat a party at this table.
	 * 
	 * @param party
	 *            the name of the party to be seated at this table.
	 */
	public void seat(String party) {
		partyName = party;
		setText("<html><center>" + tableNumber + "<br>" + party
				+ "</center></html>");
		setBackground(Color.CYAN);
		setFont(new Font("Arial", Font.PLAIN, 12));
		getModel().setPressed(true);
		SeatingManager.seat(party, tableNumber);
		state = SEATED;
		LOGGER.info("Seated " + party + " at table " + tableNumber);
	}

	/**
	 * Initialize this table. This method will set the table state to OPEN.
	 */
	private void init() {
		partyName = OPEN;
		setText("<html><center>" + tableNumber + "<br><b>" + OPEN
				+ "<b></center></html>");
		setBackground(Color.YELLOW);
		setFont(new Font("Arial", Font.BOLD, 12));
		getModel().setPressed(false);
		state = OPEN;
	}

	/**
	 * Create a pop-up dialog asking for the name of a party of customers under
	 * which a reservation should be made.
	 * 
	 * @return the name of the party of customers under which a reservation
	 *         should be made.
	 */
	private String askForReservationName() {
		String party = (String) JOptionPane.showInputDialog("Reservation for:");
		return party.trim();
	}

	/**
	 * DropBehavior establishes the drag-and-drop target listener and associated
	 * behavior.
	 * 
	 * @author sgl
	 * 
	 */
	public class DropBehavior implements DropTargetListener {
		/** The target component for the drop. */
		private JComponent comp;
		/** The original border description for the target component. */
		private Border origBorder;

		/**
		 * Constructor for this drop behanior.
		 * @param c the target component associated with this drop behavior.
		 */
		public DropBehavior(JComponent c) {
			super();
			comp = c;
		}

		/**
		 * Handle the event of a drag source entering a drop target.
		 * 
		 * @param dtde
		 *            the drag target drag event.
		 */
		@Override
		public void dragEnter(DropTargetDragEvent dtde) {
			origBorder = comp.getBorder();
			comp.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED,
					new Color(30, 60, 10).brighter(),
					new Color(30, 60, 10).darker()));
		}

		/**
		 * Handle the event of a drag source exiting a drop target.
		 * 
		 * @param dte
		 *            the drag target event.
		 */
		@Override
		public void dragExit(DropTargetEvent dte) {
			comp.setBorder(origBorder);
		}

		/**
		 * Handle the event of a drag source moving over a drop target.
		 * 
		 * @param dtde
		 *            the drop target drag event.
		 */
		@Override
		public void dragOver(DropTargetDragEvent dtde) {
		}

		/**
		 * Handle the event of a drag source being dropped on a drop target.
		 * 
		 * @param dtde
		 *            the drop target drop event.
		 */
		@Override
		public void drop(DropTargetDropEvent dtde) {
			try {
				String party = (String) dtde.getTransferable().getTransferData(
						DataFlavor.stringFlavor);
				comp.setBorder(origBorder);
				if (comp instanceof Table) {
					((Table) comp).seat(party);
					SeatingManager.seat(party, ((Table) comp).tableNumber);
				}
				dtde.dropComplete(true);
			} catch (Exception e) {
				System.err.println("ERROR: drop failed.");
			}
		}

		/**
		 * Handle the event of a drop action changing.
		 * 
		 * @param dtde
		 *            the drop target drag event.
		 */
		@Override
		public void dropActionChanged(DropTargetDragEvent dtde) {
		}
	}

	/**
	 * Handle the event of a mouse entering the component (no action).
	 * 
	 * @param arg0
	 *            (not used).
	 */
	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	/**
	 * Handle the event of a mouse exiting the component (no action).
	 * 
	 * @param arg0
	 *            (not used).
	 */
	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	/**
	 * Handle the event of a mouse press on the component (no action).
	 * 
	 * @param arg0
	 *            (not used).
	 */
	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	/**
	 * Handle the event of a mouse released on the component (no action).
	 * 
	 * @param arg0
	 *            (not used).
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	/**
	 * Seat a party.
	 * This implements the SeatingObserver.
	 * The party name is added to the table button.
	 */
	@Override
	public void seat(String party, int table) {
		if (table != tableNumber) {
			return;
		}
		partyName = party;
		setText("<html><center>" + tableNumber + "<br>" + party
				+ "</center></html>");
		setBackground(Color.CYAN);
		setFont(new Font("Arial", Font.PLAIN, 12));
		getModel().setPressed(true);
		state = SEATED;
	}

	/**
	 * Clear a table.
	 * This implements the SeatingObserver.
	 * The party name is removed from the table button and it is re-initialized.
	 */
	@Override
	public void clear(String party, int table) {
		if (table != tableNumber) {
			return;
		}
		init();
	}

	/**
	 * Add a party to the wait list.
	 * This implements the SeatingObserver.
	 * Nothing is done here.
	 */
	@Override
	public void addToWaitList(String party, int position) {
	}

	/**
	 * Remove a party from the wait list.
	 * This implements the SeatingObserver.
	 * Nothing is done here.
	 */
	@Override
	public void removeFromWaitList(String party) {
	}

	/**
	 * Update the information on the wait list.
	 * This implements the SeatingObserver.
	 * Nothing is done here.
	 */
	@Override
	public void updateWaitList(Vector<String> waitlist, int index) {
	}

}