package bop.seating;

import java.util.Vector;

/**
 * The interface for a seating observer to 
 * monitor seating and wait list status changes.
 * 
 * @see SeatingManager
 * @see Table
 * 
 * @author sgl
 *
 */
public interface SeatingObserver {

	/**
	 * Seat a party at a table.
	 * @param party the name of the party to be seated.
	 * @param table the table number at which the party is to be seated.
	 */
	public void seat(String party, int table);

	/**
	 * Clear a party from a table.
	 * @param party the name of the party which was cleared.
	 * @param table the table number which was cleard.
	 */
	public void clear(String party, int table);

	/**
	 * Add a party to the wait list.
	 * @param party the party to be added to the wait list.
	 * @param position the position in the list in which the party is added.
	 */
	public void addToWaitList(String party, int position);

	/**
	 * Remove a party from the wait list.
	 * @param party the party to be removed from the wait list.
	 */
	public void removeFromWaitList(String party);

	/**
	 * Update the wait list.
	 * If a party is removed from the middle of the wait list, only
	 * the parties affected are updated.
	 * @param waitlist the list of parties waiting to be seated.
	 * @param index the index in the list from which to start updating.
	 */
	public void updateWaitList(Vector<String> waitlist, int index);

}
