package bop.seating;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;

/**
 * A button representing a service area (non-seatable). This button is not
 * active and only provides a graphical representation of a service area in the
 * seating chart.
 * 
 * @see SeatingChart
 * 
 * @author sgl
 * 
 */
class ServiceArea extends JButton {
	/** The seraial UID */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 * 
	 * @param title
	 *            the name of the service area.
	 * @param layout
	 *            the frame layout to be used.
	 * @param x
	 *            the x coordinate for this button in the layout.
	 * @param y
	 *            the y coordinate for this button in the layout.
	 * @param height
	 *            the height of this button in the layout.
	 * @param width
	 *            the width of this button in the layout.
	 * @param pane
	 *            the pane in which to place this button.
	 */
	public ServiceArea(String title, GridBagLayout layout, int x, int y,
			int height, int width, Container pane) {
		super(title);
		this.setMargin(new Insets(1, 1, 1, 1));
		setBackground(Color.WHITE);
		setFont(new Font("Arial", Font.PLAIN, 10));
		setPreferredSize(new Dimension(Table.TABLESIZE * width, Table.TABLESIZE
				* height / 2));
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridheight = height;
		constraints.gridwidth = width;
		constraints.fill = GridBagConstraints.BOTH;
		layout.setConstraints(this, constraints);
		pane.add(this);
	}
}