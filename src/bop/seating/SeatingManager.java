package bop.seating;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import bop.common.LoggerFactory;
import bop.common.Globals;
import bop.notify.HostessNotifier;
import bop.notify.NotificationServer;
import bop.server.HTMLServer;

/**
 * The seating manager is the main interactive class for this application. The
 * seating manager contains the seating chart and wait list panels and it starts
 * any associated servers.  The embedded seating chart panel is created in the SeatingChart
 * class.  The SeatingManager implements a publish/subscribe observer pattern for
 * other classes which take actions based on changes in the wait list and seating of tables.
 * <p>
 * Global constant values are set at run time using the Globals class and execution
 * logging is handled using a logger created using the LoggerFactory.
 * 
 * @see SeatingChart
 * @see SeatingObserver
 * @see Globals
 * @see LoggerFactory
 * 
 * @author sgl
 * 
 */
/*
 * INSTRUCTOR NOTES:
 * It is recommended that you demonstrate this application in its baseline form and
 * encourage the students to play with it as well to understand the functionality
 * prior to researching the code itself.
 * 
 * As scaffolding projects for more advanced students, students might:
 * - enhance this application to handle number of people in a party and 
 * 	number of people able to be seated at a table.
 */
@SuppressWarnings("serial")
public class SeatingManager extends JPanel {
	/** The logger for this class. */
	public static final Logger LOGGER = LoggerFactory.getLogger();
	/** The business name to be displayed in the application window. */
	private static final String BIZNAME = Globals.get("BizName");
	/** The list of seating manager observers. */
	private static ArrayList<SeatingObserver> observers = new ArrayList<SeatingObserver>();
	/** The hostess notification client. */
	private static HostessNotifier HOSTESS_NOTIFIER;

	/** The main frame for the seating manager. */
	private static JFrame frame;
	/** The add button for the wait list. */
	private JButton addButton;
	/** The name entry text field for the wait list. */
	private JTextField addNameField;
	/** The displayable list for the wait list. */
	private static JList list;
	/** The wait list for customers. */
	private static Vector<String> waitingCustomers = new Vector<String>();
	/** The scrolling pane for the wait list. */
	private JScrollPane listView;

	/**
	 * Default constructor.
	 * 
	 * @param frame
	 *            the frame in which to place the seating manager.
	 */
	public SeatingManager(Container frame) {
		super();

		// start the HTML server
		LOGGER.info("Starting HTML Server");
		new HTMLServer();

		// start the notification server
		LOGGER.info("Starting Notification Server");
		new NotificationServer().start();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ie) {	}

		// start the hostess/system notification client
		LOGGER.info("Starting Hostess Notifier");
		HOSTESS_NOTIFIER = new HostessNotifier();

		// start the seating manager application
		LOGGER.info("Starting Seating Manager");
		buildGUI();

		LOGGER.info("System Ready");
	}

	/**
	 * Build the seating manager GUI frame.
	 */
	private void buildGUI() {
		// Temporary, for testing only...
		String[] arr = { "Joe", "Bill", "Sarah", "Elmo", "Henry", "Hank",
				"Charlie" };
		waitingCustomers.addAll(Arrays.asList(arr));

		// LEFT COLUMN
		// Title
		JPanel titlePanel = new JPanel();
		JLabel title = new JLabel(BIZNAME);
		title.setFont(new Font("Algerian", Font.BOLD, 40));
		title.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		titlePanel.add(title);
		add(titlePanel);

		// Seating Chart
		JPanel chartPanel = new SeatingChart();

		// Add them to the left panel and add left panel to frame.
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		leftPanel.add(titlePanel);
		leftPanel.add(chartPanel);
		add(leftPanel);

		// RIGHT COLUMN
		// Instructions
		JPanel instrPanel = new JPanel();
		JLabel instr = new JLabel();
		instr.setText("<html>Right Click: Seat/Open<br>Left Click: Reserve/Seat Reservation<br>Drag/Drop: Seat</html>");
		instr.setFont(new Font("Arial", Font.PLAIN, 10));
		instr.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		instrPanel.add(instr);

		// Add panel
		addButton = new JButton("Add");
		addNameField = new JTextField();
		addNameField.setToolTipText("add name");

		JPanel addPanel = new JPanel();
		addPanel.setBorder(BorderFactory.createTitledBorder("Add Party"));
		addPanel.setLayout(new BoxLayout(addPanel, BoxLayout.X_AXIS));
		addPanel.add(addButton);
		addPanel.add(addNameField);

		// create an action listener to add the name in the wait list
		// text field to the wait list when the add button is clicked.
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String party = addNameField.getText().trim();
				addNameField.setText("");
				// do not allow an empty name
				if (party.length() > 0) {
					addToWaitList(party);
				}
			}

		});

		// create an action listener to add the name in the wait list
		// text field to the wait list when a return is pressed in the text
		// field.
		addNameField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String party = addNameField.getText().trim();
				addNameField.setText("");
				// do not allow an empty name
				if (party.length() > 0) {
					addToWaitList(party);
				}
			}
		});

		// Wait list
		list = new JList(waitingCustomers);
		// set drag behavior
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setDragEnabled(true);
		listView = new JScrollPane(list);
		listView.setBorder(BorderFactory.createTitledBorder("Wait List"));

		JPanel notifierPanel = HOSTESS_NOTIFIER.getPanel();
		notifierPanel.setBorder(BorderFactory
				.createTitledBorder("Message Center"));

		// Add them to the right panel and add right panel to frame.
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		rightPanel.add(instrPanel);
		rightPanel.add(addPanel);
		rightPanel.add(listView);

		rightPanel.add(notifierPanel);

		add(rightPanel);
	}

	/**
	 * Is the given party on the wait list.
	 * @param party the party to be checked
	 * @return <code>true</code> if the party is on the wait list, 
	 * <code>false</code> otherwise.
	 */
	public static boolean isWaiting(String party) {
		return waitingCustomers.contains(party);
	}

	/**
	 * Add a name to the wait list. This will add the name to the list and
	 * update the wait list display.
	 * 
	 * @param party the party to add to the wait list.
	 */
	public static void addToWaitList(String party) {
		// the name of the party must be unique to be added.
		if (waitingCustomers.contains(party)) {
			return;
		}
		waitingCustomers.addElement(party);
		list.repaint();
		LOGGER.info("Added " + party + " to waitlist");
		int count = waitingCustomers.size();
		notitfyObserversAddToWaitList(party, count);
	}

	/**
	 * Seat a party at a specified table.
	 * 
	 * @param party
	 *            the part to seat.
	 * @param tableNumber
	 *            the table number to seat the party at.
	 */
	public static void seat(String party, int tableNumber) {
		// remove the party from the wait list.
		waitingCustomers.remove(party);
		LOGGER.info("Seated " + party + " at table " + tableNumber);
		list.repaint();
		notitfyObserversSeat(party, tableNumber);
	}

	/**
	 * Clear a table. Once the party has finished, clear the table for use by
	 * another party.
	 * 
	 * @param party
	 *            the party being cleared.
	 * @param tableNumber the number identifying the table being cleared.
	 */
	public static void clear(String party, int tableNumber) {
		LOGGER.info("Cleared " + party + " from table " + tableNumber);
		notitfyObserversClear(party, tableNumber);
	}

	/**
	 * Remove a party from the wait list.
	 * 
	 * @param party
	 *            the party to be removed from the wait list.
	 */
	public static void removeFromWaitList(String party) {
		waitingCustomers.remove(party);
		list.repaint();
		LOGGER.info("Removed " + party + " from waitlist");
		notifyObserversRemoveFromWaitList(party);
	}

	/**
	 * Get the next party to be seated in the wait list.
	 * 
	 * @return the name of the party that is first on the wait list.
	 */
	public static String getNextParty() {
		if (waitingCustomers.size() == 0) {
			return null;
		}
		return waitingCustomers.firstElement();
	}

	/**
	 * Create the GUI and display it. For thread safety, this method should be
	 * invoked from the event-dispatching thread.
	 */
	private static void createAndShowGUI() {

		// Create and set up the window.
		frame = new JFrame("Seating Manager");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		JComponent newContentPane = new SeatingManager(frame);
		newContentPane.setOpaque(true); // content panes must be opaque
		frame.setContentPane(newContentPane);

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	// SeatingObserver methods
	/**
	 * Register an observer with the seating manager.
	 * @param observer the class to be registered.
	 */
	public static void registerObserver(SeatingObserver observer) {
		observers.add(observer);
	}

	/**
	 * Remove an observer from the seating manager.
	 * @param observer the class to be removed.
	 */
	public static void removeObserver(SeatingObserver observer) {
		observers.remove(observer);
	}

	/**
	 * Notify all observers that a party has been added to the wait list.
	 * @param party the party which was added to the wait list.
	 * @param position the position in the wait list at which the party was added.
	 */
	public static void notitfyObserversAddToWaitList(String party, int position) {
		for (SeatingObserver observer : observers) {
			observer.addToWaitList(party, position);
		}
	}

	/**
	 * Notify all observers that a party has been removed from the wait list.
	 * @param party the party to be removed from the wait list.
	 */
	public static void notifyObserversRemoveFromWaitList(String party) {
		for (SeatingObserver observer : observers) {
			int index = waitingCustomers.indexOf(party);
			observer.removeFromWaitList(party);
			observer.updateWaitList(waitingCustomers, index);
		}
	}

	/**
	 * Notify all observers that a party has been seated at a table.
	 * @param party the party which was seated.
	 * @param table the table at which the party was seated.
	 */
	public static void notitfyObserversSeat(String party, int table) {
		for (SeatingObserver observer : observers) {
			observer.seat(party, table);
			observer.updateWaitList(waitingCustomers, 0);
		}
	}

	/**
	 * Notify all observers that a table has been cleared.
	 * @param party the party which was previously seated at the table.
	 * @param table the table which was cleared.
	 */
	public static void notitfyObserversClear(String party, int table) {
		for (SeatingObserver observer : observers) {
			observer.clear(party, table);
		}
	}

	// End of Observer methods.

	/**
	 * The main entry point for the seating manager application.
	 * 
	 * @param args
	 *            (not used)
	 */
	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
