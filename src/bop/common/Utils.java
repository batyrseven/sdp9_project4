package bop.common;

public class Utils {
    public static String byteArrayToHexString(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private static String byteArrayToHexString_deprecated(byte [] b) {
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i=0;i<b.length;i++) {
            int v = b[i] & 0xff;
            if (v < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString().toUpperCase();
    }

    private static byte[] hexStringToByteArray_deprecated(String s) {
        byte [] b = new byte[s.length() / 2];
        for (int i=0;i<b.length;i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
        }
        return b;
    }
}
