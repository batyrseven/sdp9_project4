package bop.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Globals uses a properties file to allow for runtime configuration of global
 * variables used in this application.  This class is used to set constant values
 * from a properties resource file which can then be used globally by any class.
 * 
 * @author sgl
 * 
 */
public class Globals {

	/** The key/value properties for the globals. */
	private static Properties props = new Properties();

	// Load the property values from the properties file.
	static {
		String fname = "bop.properties";
		File f = new File(fname);
		try {
			FileInputStream in = new FileInputStream(f);
			props.load(in);
		} catch (IOException e) {
			try {
				fname = f.getCanonicalPath();
			} catch (IOException ioe) {
			}
			;
			System.err.println("Could not open " + fname + ": "
					+ e.getMessage());
		}
	}

	/**
	 * Get the value of a global variable given its key.
	 * 
	 * @param key
	 *            the key for the global value.
	 * @return the value for the specified key.
	 */
	public static String get(String key) {
		return props.getProperty(key);
	}
}
