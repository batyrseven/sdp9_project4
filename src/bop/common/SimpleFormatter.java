package bop.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * A simple logging formatter. This formatter write log entries to a single
 * time-stamped line.
 * 
 * @see LoggerFactory
 * 
 * @author sgl
 * 
 */
class SimpleFormatter extends Formatter {
	/** The line separator character (system dependent) */
	private static final String LINE_SEPARATOR = System
			.getProperty("line.separator");

	/**
	 * Format the given log record for a text file.
	 * 
	 * @param rec
	 *            the log record to format.
	 * @return a formatted string of the log record.
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 */
	public String format(LogRecord rec) {
		StringBuilder sb = new StringBuilder();
		sb.append(calcDate(rec.getMillis())).append("\t" + formatMessage(rec));
		if (rec.getLevel().intValue() >= Level.WARNING.intValue()) {
			sb.append("\t***** " + rec.getLevel());
		}
		sb.append(LINE_SEPARATOR);
		return sb.toString();
	}

	/**
	 * Format the given date in the form of a long into a string.
	 * 
	 * @param millisecs
	 *            the date/time in the form of milliseconds.
	 * @return a string representation of the date and time.
	 */
	private String calcDate(long millisecs) {
		SimpleDateFormat date_format = new SimpleDateFormat("MMM dd,yyyy HH:mm");
		Date resultdate = new Date(millisecs);
		return date_format.format(resultdate);
	}

}