package bop.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * This html formatter logs records to a single line in html.
 * 
 * @see LoggerFactory
 * 
 * @author sgl
 * 
 */
class HtmlFormatter extends Formatter {

	/**
	 * Format the given log record to an html format.
	 * 
	 * @param rec
	 *            the logging record to format.
	 * @return a string containing the html formatted log record.
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 */
	@Override
	public String format(LogRecord rec) {
		StringBuffer buf = new StringBuffer(1000);
		// Bold any levels >= WARNING
		buf.append("<tr>");
		buf.append("<td>");

		if (rec.getLevel().intValue() >= Level.WARNING.intValue()) {
			buf.append("<b>");
			buf.append(rec.getLevel());
			buf.append("</b>");
		} else {
			buf.append(rec.getLevel());
		}
		buf.append("</td>");
		buf.append("<td>");
		buf.append(calcDate(rec.getMillis()));
		buf.append("</td>");
		buf.append("<td>");
		buf.append(formatMessage(rec));
		buf.append("</td>");
		buf.append("</tr>\n");
		return buf.toString();
	}

	/**
	 * Format the given date in the form of a long into a string.
	 * 
	 * @param millisecs
	 *            the date/time in the form of milliseconds.
	 * @return a string representation of the date and time.
	 */
	private String calcDate(long millisecs) {
		SimpleDateFormat date_format = new SimpleDateFormat("MMM dd,yyyy HH:mm");
		Date resultdate = new Date(millisecs);
		return date_format.format(resultdate);
	}

	/**
	 * Create the html head block for the given html file handler.
	 * 
	 * @param h
	 *            the html file handler.
	 * @return the head block for the html file.
	 */
	@Override
	public String getHead(Handler h) {
		return "<HTML>\n<HEAD>\n" + (new Date()) + "\n</HEAD>\n<BODY>\n<PRE>\n"
				+ "<table width=\"100%\" border>\n  " + "<tr><th>Level</th>"
				+ "<th>Time</th>" + "<th>Log Message</th>" + "</tr>\n";
	}

	/**
	 * Create the html tail block for the given html file handler.
	 * 
	 * @param h
	 *            the html file handler.
	 * @return the tail block for the html file.
	 */
	@Override
	public String getTail(Handler h) {
		return "</table>\n  </PRE></BODY>\n</HTML>\n";
	}
}