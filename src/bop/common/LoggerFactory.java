package bop.common;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A common logging utility class.
 * 
 * This class creates new loggers which log to a text file as well as an html
 * file.
 * 
 * @see SimpleFormatter
 * @see HTMLFormatter
 * 
 * @author sgl
 * 
 */
public class LoggerFactory {
	/** A text file handler for this logger. */
	static private FileHandler fileTxt;
	/** A text formatter for this logger. */
	static private SimpleFormatter formatterTxt;
	/** An html file handler for this logger. */
	static private FileHandler fileHTML;
	/** An html formatter for this logger. */
	static private Formatter formatterHTML;
	/** The singleton logger returned from this factory */
	static Logger logger = null;

	/**
	 * Return a singleton logger for logging execution information
	 * in both text and html formats.
	 * 
	 * @return a singleton logger.
	 */
	static public Logger getLogger() {
		String loggerName = "BOP";
		// Get the global logger to configure it
		// Create a singleton logger (only one instance).
		if (logger == null) {
			logger = Logger.getLogger(loggerName);
		}

		logger.setLevel(Level.INFO);
		logger.setUseParentHandlers(false);

		try {
			fileTxt = new FileHandler("log/" + loggerName + "Log.txt", 65536,
					2, true);
			fileHTML = new FileHandler("webdocs/" + loggerName + "Log.html");
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Problems creating the log files");
		}

		// Create txt Formatter
		formatterTxt = new SimpleFormatter();
		fileTxt.setFormatter(formatterTxt);
		logger.addHandler(fileTxt);

		// Create HTML Formatter
		formatterHTML = new HtmlFormatter();
		fileHTML.setFormatter(formatterHTML);
		logger.addHandler(fileHTML);

		return logger;
	}

}
